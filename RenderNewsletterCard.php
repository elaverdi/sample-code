<?php
class Zend_View_Helper_RenderNewsletterCard extends Zend_View_Helper_Abstract {

  public function renderNewsletterCard($newsletter, $featured = false) {

		if(!empty($newsletter)):
			$class = array();

			if(!empty($this->view->status)) {
				$class[] = $this->view->status;
				if($newsletter['following']){
					$class[] = 'following';
				}
			}
		?>
			<div class="columns <?=($featured) ? 'featured-column':''?>">

				<div class="card newsletter <?=implode(' ', $class);?>"
						 data-id="<?=$newsletter['id'];?>"
						 data-esp="<?=$newsletter['esp'];?>"
						 data-email="<?=$this->view->email;?>"
						 data-sitekey="<?=$this->view->sitekey?>"
						 data-status="<?=$this->view->status?>"
						 data-equalizer-watch>
					<div class="recaptcha"></div>

					<div class="cover" style="background-image: url('<?=$newsletter['image'];?>');">
						<?php if(!empty($this->view->status)): ?>
						<a class="button control" href="">
							<i class="fa fa-check"></i>
							<i class="fa fa-plus"></i>
						</a>
						<?php endif; ?>
					</div>

					<div class="description">
						<label data-date-created = "<?=$newsletter['date']?>">
							<?=$this->view->renderNewTitle($newsletter['date'])?> <?=$newsletter['frequency'];?>
						</label>
						<h5 class="card-title"><?=$newsletter['name'];?></h5>
						<p class="txt-l1"><?=$newsletter['description'];?></p>
					</div>

					<div class="action">
						<?php if(!empty($this->view->status)): ?>
						<div class="follow"><a class="button expanded control" href="#follow">FOLLOW</a></div>
						<div class="following"><a class="button secondary expanded control" href="#unfollow">FOLLOWING</a></div>
						<?php else: ?>
						<form class="email-form">
							<div class="input-group">
							<input class="input-group-field" type="text" placeholder="Email Address">
							<div class="input-group-button">
								<button class="button" type="submit" href="">
									<i class="fa fa-angle-right"></i>
									<i class="fa fa-exclamation-circle"></i>
								</button>
								<span class="error-message">Please input a valid email address.</span>
							</div>
						</div>
						</form>
						<?php endif; ?>
					</div>

				</div>
			</div>
    <?php endif;
  }
}
?>
