
## Newsletter Landing Page Application

---

This code is only to be used for reference and contains sample code used on an application I wrote. Sample work can be viewed https://membership.latimes.com/newsletters/.

1. **_newsletters.scss** contains the pre-compiled css for the application.
2. **newsletters.js** containers the javascript that runs the newsletter application. ( The filter menu, the banner, and the newsletters subscriptions)
3. **NewslettersController** manages the execution flow of the request to the view.
4. **newsletters.phtml** contains the view.
5. **RenderNewsletterCard** and **RenderNewsletterMenu** are the view helpers utilized in the view phtml file.
