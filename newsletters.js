$(function() {

  $(document).foundation();
  $('.master-slider').masterslider({ speed: 100, start: 1, width: 300, layout: 'partialview', space: 10, view: 'basic', loop: false, autoHeight: true });
  $('div.columns.filter').filterMenu();
  $('div.columns.featured-column').featuredSwitch();
  $('section#newsletters').enableNewsletters();

});

////////////////////////////////
// FEATURED NEWSLETTER SWITCH //
////////////////////////////////

$.fn.featuredSwitch = function () {
  $column = $(this);
  var $parent = $column.parent('section');
  var size = $parent.find('.newsletter').size();

  if (size > 1) {
    var moveToBanner = function() {
      $column.appendTo('#banner .row').find('.card').removeAttr('data-equalizer-watch').removeAttr('style');
      Foundation.reInit('sticky');
      Foundation.reInit('equalizer');
    };

    var moveToBody = function() {
      $column.prependTo($parent).find('.card').attr('data-equalizer-watch', '');
      $parent.find('h4').prependTo($parent);
      Foundation.reInit('sticky');
      Foundation.reInit('equalizer');
    };

    if (Foundation.MediaQuery.atLeast('xlarge')) {
      moveToBanner();
    }

    $(window).resize(function() {
      $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
        if (newSize === 'xlarge' && oldSize === 'large') {
          moveToBanner();
        }
        if (newSize === 'large' && oldSize === 'xlarge') {
          moveToBody();
        }
      });
    });
  }
  else {
    $('.hero.featured').removeClass('featured');
    if($('.filter').length !== 0) {
      Foundation.reInit('sticky');
    }
  }
}
//////////////////////////////////////////////////////////
// FILTER MENU USED ON USED ON NEWSLETTERS LANDING PAGE //
//////////////////////////////////////////////////////////

$.fn.filterMenu = function () {
  var $filter 	= $(this);

  var $openFilter 	= $filter.find('.filter-button');
  var $applyFilter 	= $filter.find('.apply-button');
  var $closeFilter 	= $filter.find('.close-button');

  var $filterLinks 		= $filter.find('.filter-link');
  var $filterAll = $filter.find('.filter-link-all');
  var $filterMy 	= $filter.find('.filter-link-my');

  var $sections = $('#newsletters section');
  var $sectionsTemp = null;
  var selectedSize = null;

  // OPEN FILTER FUNCTIONALITY
  $openFilter.click( function(e) {
    e.preventDefault();
    $('html').toggleClass('filter-open');
    $sectionsTemp = $filter.find('.filter-link, .filter-link-all, .filter-link-my').filter('.selected');
  });

  // APPLY FILTER FUNCTIONALITY
  $applyFilter.click( function(e) {
    e.preventDefault();
    $('html').toggleClass('filter-open');
    $sectionsTemp = null;
  });

  // CLOSE FILTER FUNCTIONALITY
  $closeFilter.click( function(e) {
    e.preventDefault();
    $filterAll.click();
    $sectionsTemp.each(function() {
      $(this).click();
    });
    $('html').toggleClass('filter-open');
    $sectionsTemp = null;
  });

  // VIEW ALL NEWSLETTERS FUNCTIONALITY
  $filterAll.click( function(e){
    e.preventDefault();
    $filterLinks.removeClass('selected');
    $filterMy.removeClass('selected');
    $filterAll.addClass('selected');
    $sections.addClass('selected').filter('.section-0').removeClass('selected');
    $('#section-0').foundation();
    Foundation.reInit('equalizer');
    Waypoint.refreshAll();
    if($('#banner').hasClass('featured'))
      scrollFilter('#banner');
    else
      scrollFilter('#newsletters');
  });

  // VIEW MY NEWSLETTERS FUNCTIONALITY
  $filterMy.click( function(e){
    e.preventDefault();
    var $this = $(this);
    $filterAll.removeClass('selected');
    $filterLinks.removeClass('selected');
    $this.addClass('selected');
    var current = $this.attr('href').replace('#','.');
    $sections.removeClass('selected').filter(current).addClass('selected');
    $('#section-0').foundation();
    Foundation.reInit('equalizer');
    Waypoint.refreshAll();
    scrollFilter('#newsletters');
  });

  // VIEW SELECTED NEWSLETTERS FUNCTIONALITY
  $filterLinks.click( function(e){
    e.preventDefault();
    var $currentLink = $(this);
    var current = $currentLink.attr('href').replace('#','.');

    // SET CORRECT STATE FOR MENU
    $filterAll.removeClass('selected');
    $filterMy.removeClass('selected');
    $currentLink.toggleClass('selected');

    // GET COUNT OF SELECTED FROM MENU
    selectedSize = $filterLinks.filter('.selected').size();

    // INTIATE FIRST STATE OF FILTER
    if ( selectedSize === 1 && $currentLink.hasClass('selected') ) {
      $sections.removeClass('selected').filter(current).addClass('selected');
      scrollFilter(current);
    }
    // IF NONE ARE SELECTED VIEW ALL
    else if ( selectedSize === 0 ) {
      $filterAll.click();
    }
    // TOGGLE SELECTED NEWSLETTER
    else {
      $sections.filter(current).toggleClass('selected');
      scrollFilter(current);
    }
    Foundation.reInit('equalizer');
    Waypoint.refreshAll();
  });

  // SCROLL TO FUNCTIONALITY
  var scrollFilter = function(to) {
    if (Foundation.MediaQuery.atLeast('xlarge')) {
      $('html, body').animate({
        scrollTop: $(to).offset().top
      }, 1000);
    }
  };

  // ON RESIZE CLOSE FILTER MENU
  $(window).resize(function() {
    $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
      if (newSize === 'xlarge' && oldSize === 'large' && $('html').hasClass('filter-open')) {
        $closeFilter.click();
      }
    });
  });
}

///////////////////////////////////
// ENABLE NEWSLETTERS FORM //
///////////////////////////////////

$.fn.enableNewsletters = function () {

  var $myNewsletters = $('.section-0');
  var currentDefaults;
  var displayEmail = false;
  var count = 0;
  var processing = false;

  // ON FOCUS CLEAR INPUT CONTENT
  $('.action .email-form .input-group .input-group-field').focusin(function() {
    if(!processing) {
      $('.action .input-group').removeClass('active');
      $(this).parent().addClass('active');

      $('.action .input-group:not(.active) .input-group-field').val('');
      if (displayEmail) {
        if (typeof currentDefaults.email !== typeof undefined && currentDefaults.email !== false ) {
          $('.action .input-group.active .input-group-field').val(currentDefaults.email);
        }
      }
      if ($('.action .input-group.active .input-group-field').val() === '') {
        $('.action .input-group.active').removeClass('alert');
      }
    }
  });
  // ON NOT LOGGED IN EMAIL SUBMITION
  $('.action .email-form').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    var $inputGroup = $form.find('.input-group');
    if ($inputGroup.hasClass('active')) {
      var email = $form.find('input').val();
      // NOT EMAIL NOT VALID ALERT
      if(isValidEmailAddress(email)){
        $inputGroup.removeClass('alert');
        $(".action .email-form input").prop("disabled", true);
        $form.parents('.newsletter').attr('data-email', email);
        subscribe($form.find('.button'), 'subscribe');
      }
      else {
        $inputGroup.addClass('alert');
      }
    }
  });
  // TYPE AS YOU CLICK VALIDATION
  $('.action .email-form .input-group .input-group-field').on('input', function() {
    var $email = $(this);
    if ($email.parent().hasClass('alert')) {
      if(isValidEmailAddress($email.val())){
        $email.parent().removeClass('alert');
      }
    }
  });
  // FOLLOW/UNFOLLOW FOR USERS NOT LOGGED IN
  $('.newsletter a.button.control' ).on('click', function(e) {
    e.preventDefault();
    var action = ($(this).parents('.newsletter').hasClass('following')) ? 'unsubscribe' : 'subscribe';
    subscribe($(this), action);
  });
  // EMAIL VALIDATION
  var isValidEmailAddress = function (emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
  };

  // process unsubscribe/subscribe for logged and bypass results
  var processSubscription = function( $newsletter, result, params) {

    $newsletter.find('a.button').removeClass('disabled');
    // IF FOLLOW BUTTON CLICKED
    if (params.method === 'subscribe') {
      if (result.code == '1') {
        // UPDATE NEWSLETTER
        $("[data-id='"+params.id+"']").addClass('following');
        // UPDATE MY NEWSLETTERS IF NEWSLETTER DOES NOT EXIST
        if (!$(".section-0 [data-id='"+params.id+"']").length) {
          // ADD TO MY NEWSLETTERS IPHONE OR ANDROID PHONES
          if(checkSupport.isAndroidPhone() || checkSupport.isiPhone()) {
            // STORE CURRENT NEWLETTERS
            var $newslettersTemp = $myNewsletters.find('[data-id]');
            // RECREATE IT SLIDER
            var $masterslider = $('<div class="master-slider ms-skin-default" data-equalizer data-equalize-by-row="true"></div>');
            // ADD NEWEST NEWSLETTER TO FRONT SLIDE
            $newsletter.parent().clone(true).appendTo($masterslider).wrap('<div class="ms-slide"></div>');
            // ADD ADD STORED NEWSLETTERS
            $newslettersTemp.each(function( key, nl ) {
              $(nl).parent().clone(true).appendTo($masterslider).wrap('<div class="ms-slide"></div>');
            });
            // REINTITIALIZE DATA
            $myNewsletters.find('.master-slider').masterslider('destroy');
            $myNewsletters.append($masterslider);
            $masterslider.masterslider({ speed: 100, start: 1, width: 300, layout: 'partialview', space: 10, view: 'basic', loop: false, autoHeight: true });
            $myNewsletters.foundation();
            Foundation.reInit('equalizer');
          }
          // ADD TO MY NEWSLETTERS DESKTOP
          else {
            if ($myNewsletters.find('[data-id]').size() === 0) {
              $myNewsletters.append($newsletter.parent().clone(true));
            }
            else{
              $myNewsletters.append($newsletter.parent().clone(true));
              $myNewsletters.attr('data-equalizer', '').attr('data-equalize-by-row', 'true');
              Foundation.reInit('equalizer');
            }
          }
        }
      }
    }
    // IF UNFOLLOW BUTTON CLICKED
    else {
      // UPDATE NEWSLETTER
      $("[data-id='"+params.id+"']").removeClass('following');
      if ($(".section-0 [data-id='"+params.id+"']").length) {
        // REMOVE FROM MY NEWSLETTERS IPHONE ANDROID PHONE
        if(checkSupport.isAndroidPhone() || checkSupport.isiPhone()) {
          // REMOVE SELECTED NEWSLETTER
          $(".section-0 [data-id='"+params.id+"']").parents('.ms-slide').remove();
          // STORE CURRENT NEWLETTERS
          var $newslettersTemp = $myNewsletters.find('[data-id]');
          // RECREATE IT SLIDER
          var $masterslider = $('<div class="master-slider ms-skin-default" data-equalizer data-equalize-by-row="true"></div>');
          // ADD ADD STORED NEWSLETTERS
          $newslettersTemp.each(function( key, nl ) {
            $(nl).parent().clone(true).appendTo($masterslider).wrap('<div class="ms-slide"></div>');
          });
          $myNewsletters.find('.master-slider').masterslider('destroy');
          // REINTIALIZE ONLY IF NEWSLETTERS ARE STILL PRESENT
          if ($newslettersTemp.size() > 0) {
            $myNewsletters.append($masterslider);
            $masterslider.masterslider({ speed: 100, start: 1, width: 300, layout: 'partialview', space: 10, view: 'basic', loop: false, autoHeight: true });
            $myNewsletters.foundation();
            Foundation.reInit('equalizer');
          }
        }
        // REMOVE FROM MY NEWSLETTERS DESKTOP
        else {
          $(".section-0 [data-id='"+params.id+"']").parent().fadeOut( function() {
            $(this).remove();
            if ($myNewsletters.find('[data-id]').size() === 0) {
              $myNewsletters.removeAttr('data-equalizer').removeAttr('data-equalize-by-row').removeAttr('data-resize');
            }
            else {
              Foundation.reInit('equalizer');
            }
          });
        }
      }
    }
  }

  // SUBSCRIBE/UNSUBSCRIBE TO EMAILS ACTION
  var subscribe = function($obj, method) {

    var defaults = {
      id: null, esp: null, email: null, token: null, sitekey: null, rid: null, method: null, loggedin: null, status: null
    };

    processing = true;
    var $newsletter = $obj.parents('.newsletter');
    $newsletter.find('a.button').addClass('disabled');

    defaults.id = $newsletter.data('id');
    defaults.esp = $newsletter.data('esp');
    defaults.email = $newsletter.data('email');
    defaults.sitekey = $newsletter.data('sitekey');
    defaults.status = $newsletter.data('status');
    defaults.method = method;

    // ONLY IF USER IS LOGGED IN
    if (defaults.status === 'loggedin' || (defaults.status === 'loggedin bypass' && defaults.method === 'unsubscribe')) {

      $.ajax({
          url: '/newsletters/subscription/',
          type: 'POST',
          dataType: 'json',
          data: { id: defaults.id, esp: defaults.esp, method: defaults.method, email: defaults.email, status: defaults.status },
          success: function(result) {
            processSubscription($newsletter, result, defaults);
        }
      });
    }
    else {
      // START RECAPTCHA
      var rid = $newsletter.data('rid');
      if (typeof rid !== typeof undefined && rid !== false ) {
        defaults.rid = rid;
      }
      else {
        defaults.rid = grecaptcha.render($newsletter.find('.recaptcha')[0], {'sitekey': defaults.sitekey, 'size': 'invisible', 'callback': 'captchaSubmit', 'badge': 'inline'});
        $newsletter.attr('data-rid', defaults.rid);
      }
      currentDefaults = defaults;
      $('.action .input-group').removeClass('.active');
      grecaptcha.execute(defaults.rid);
    }
  };

  // FORM SUBMIT FOR RECAPTCHA
  var submitForm = function() {
    var response = currentDefaults.rid;

    if(currentDefaults.status === 'loggedin bypass') {
      if (currentDefaults.token.length) {

        var $newsletter = $('div.newsletter[data-id="'+currentDefaults.id+'"]');
        $newsletter.find('a.button').addClass('disabled');

        $.ajax({
            url: '/newsletters/subscription/',
            type: 'POST',
            dataType: 'json',
            data: { id: currentDefaults.id, esp: currentDefaults.esp, email: currentDefaults.email, token: currentDefaults.token, method: currentDefaults.method, status: currentDefaults.status },
            success: function(result) {
              processSubscription($newsletter, result, currentDefaults);
            }
        });
      }
    }
    else {
      var $active = $('.action .active');
      $active.find('.action .active .button').addClass('disabled');
      if (currentDefaults.token.length) {
        $.ajax({
            url: '/newsletters/subscription/',
            type: 'POST',
            dataType: 'json',
            data: { id: currentDefaults.id, esp: currentDefaults.esp, email: currentDefaults.email, token: currentDefaults.token, method: currentDefaults.method, status: false },
            success: function(result) {
              var message = '';
              displayEmail = true;
              if (result.code == '1') {
                count++;
                message = (count > 1)? 'Now following. Create a <a href="#" data-reg-handler="show-register">free account</a> to manage your newsletters.' : result.message;
              }
              else {
                message = result.message;
              }
              $active.parents('.action').html('<div class="txt-l2">'+message+'</div>');
              $('.action .email-form input').prop('disabled', false);;
              processing = false;
            }
        });
      }
    }
    return response;
  };

  return {
    setToken: function(token) {
      currentDefaults.token = token;
      return submitForm();
    }
  };
}
