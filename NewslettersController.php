<?php
class NewslettersController extends Zend_Controller_Action {

  public function preDispatch() {

  }

  public function indexAction(){
    $config = Zend_Registry::get('config');


    $request = $this->getRequest();
    $email = $request->getParam('u');
    $content = Application_Model_Feed::create('newsletters');

    // IF USER VAR IS NOT EMPTY AND USER VAR IS ACTUAL EMAIL FORMAT REDIRECT RESPONSE WITH ENCRYPTED EMAIL
    if(!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $baseUrl = new Zend_View_Helper_BaseUrl();
      $this->getResponse()->setRedirect($baseUrl->baseUrl().'/newsletters/index/u/'.$content->encrypt($email));
      return;
    }
    elseif (!empty($email) && !filter_var($content->decrypt($email), FILTER_VALIDATE_EMAIL)) {
      $baseUrl = new Zend_View_Helper_BaseUrl();
      $this->getResponse()->setRedirect($baseUrl->baseUrl().'/newsletters');
      return;
    }
    // ELSE USER LOAD PAGE WITH KNOWN DATA
    else {

      $request = $this->getRequest();
      $config = Zend_Registry::get('config');
      $user = Zend_Registry::get('user');
      $isLoggedIn = Zend_Registry::get('isloggedin');

      $this->view->headTitle($config['market']['title'].' - Newsletters');
      $description = (!empty($config['meta']['newsletters']['description'])) ? $config['meta']['newsletters']['description'] : '';
      $keywords = (!empty($config['meta']['newsletters']['keywords'])) ? $config['meta']['newsletters']['keywords'] : '';
      $this->view->headMeta()->setName('description', $description);
      $this->view->headMeta()->setName('keywords', $keywords);

      $email = null;

      if($isLoggedIn) {
        $email = $user->getEmail();
        $this->view->assign('status', 'loggedin');
      }
      elseif (!empty($request->getParam('u'))) {
        $email = $request->getParam('u');
        $email = $content->decrypt($email);
        $this->view->assign('status', 'loggedin bypass');
      }
      else {
        $this->view->assign('status', null);
      }

      $newsletters  = $content->getNewsletters($email);
      $this->view->assign('newsletters', $newsletters);
      $this->view->assign('email', $email);

      $bannerType = $config['newsletters']['banner'];
      $bannerType = (!empty($bannerType)) ? $bannerType : 'featured';
      $this->view->assign('marketData', $content->getMarket());
      $this->view->assign('heading', $config['market']['title'].' Newsletters');
      $this->view->assign('marketName', $config['market']['title']);
      $this->view->assign('bannerType', $bannerType);
      $this->view->assign('sitekey', $config['recaptcha']['sitekey']);

      if(!empty($config['newsletters']['sponsor'])) {
        $this->view->assign('sponsor', $config['newsletters']['sponsor']);
      }
      else {
        $this->view->assign('sponsor', null);
      }

      $this->renderScript( 'pages/newsletters.phtml' );
      return;
    }
  }

  public function subscriptionAction() {

    if($this->getRequest()->isPost()) {

      $config = Zend_Registry::get('config');
      $request = $this->getRequest();

      $id = $request->getParam('id');
      $esp = $request->getParam('esp');
      $method = $request->getParam('method');
      $email = $request->getParam('email');
      $status = $request->getParam('status');
      $token = $request->getParam('token');
      $content = Application_Model_Feed::create('newsletters');

      if($status === 'loggedin') {
        $result = $content->subscribeUser($id, $esp, $method, $email, $status);
      }
      elseif($status === 'loggedin bypass') {
        $result = $content->subscribeUser($id, $esp, $method, $email, $status, $token);
      }
      else {
        /// EMAIL 1394 Remove check if registered
        $isRegistered = $content->findUserByEmail($email);
        /*$ids = $content->getUsersNewslettersIds($email);
        $id = intval($id);

        if(in_array($id, $ids)) {
          $result['code'] = '10';
          if($isRegistered){
            $result['message'] = 'This email is already following this newsletter. <a href="#" data-reg-handler="signInHandler">Log in</a> to manage all your newsletters.';
          }
          else {
            $result['message'] = 'This email is already following this newsletter.';
          }
          $result = json_encode($result);
        }
        else {*/
          $result = $content->subscribeUser($id, $esp, $method, $email, $status, $token);
          $result = json_decode($result, true);
          if($isRegistered && $result['processed']){
            $result['code'] = '10';
            $result['message'] = 'Now following. <a href="#" data-reg-handler="signInHandler">Log in</a> to manage all your newsletters.';
          }
          else if(!$isRegistered && $result['processed']) {
            $result['message'] = 'You will now start receiving this newsletter!';
          }
          else {
            $newrelic = Zend_Registry::get('newrelic');
            $newrelic->setNoticeError("Internal subscription Error (source,esp,email,method) -> ($source,$esp,$email,$method)");
          }
          $result = json_encode($result);
        //}
      }

      $this->_helper->layout->setLayout('service');
      $this->view->assign('response', $result);
      $this->renderScript( 'pages/service.phtml' );
      return;
    }
    else {
      $this->_redirect('/newsletters/');
    }
  }

  public function preferencesAction() {
    $config = Zend_Registry::get('config');


    $request = $this->getRequest();
    $email = $request->getParam('u');
    $content = Application_Model_Feed::create('newsletters');

    // IF USER VAR IS NOT EMPTY AND USER VAR IS ACTUAL EMAIL FORMAT REDIRECT RESPONSE WITH ENCRYPTED EMAIL
    if(!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {

      $baseUrl = new Zend_View_Helper_BaseUrl();
      $this->getResponse()->setRedirect($baseUrl->baseUrl().'/newsletters/preferences/u/'.$content->encrypt($email));
      return;
    }
    // ELSE USER LOAD PAGE WITH KNOWN DATA
    else {

      $user = Zend_Registry::get('user');

      // IF USER IS LOGGED IN DECRYPT LOGGED IN EMAIL
      if($user->isLoggedIn()) {
        $emailReq = $email;
        $email = $user->getEmail();
        if(!empty($emailReq)) {
          $emailReq = $content->decrypt($emailReq);
          if($email != $emailReq) {
            $this->view->assign('emailReq', $emailReq);
          }
        }
      }
      else {
        $email = $content->decrypt($email);
      }
      if(!empty($email)) {

        $this->view->assign('marketTitle', $config['market']['title']);
        $this->view->assign('bannerType', $config['newsletters']['banner']);
        $this->view->assign('marketData', $content->getMarket());
        $this->view->assign('preferences', $content->getPreferences($email));
        $this->view->assign('email', $email);
        $this->renderScript( 'pages/newsletters-preferences.phtml' );
        return;
      }
      else {
        $this->_redirect('/newsletters/');
        return;
      }
    }
  }

  public function savePreferencesAction() {

    if($this->getRequest()->isPost()) {

      $config = Zend_Registry::get('config');
      $request = $this->getRequest();
      $content = Application_Model_Feed::create('newsletters');

      $email = $request->getParam('u');

      if(!empty($request->getParam('unsubscribe')) && filter_var($request->getParam('unsubscribe'), FILTER_VALIDATE_BOOLEAN)) {
        $fields = array(
          array( 'adv' => false),
          array( 'store' => false),
          array( 'events' => false),
          array( 'acq' => false)
        );
      }
      else {
        $fields = array(
          array( 'adv' => filter_var($request->getParam('ads'), FILTER_VALIDATE_BOOLEAN)),
          array( 'store' => filter_var($request->getParam('store'), FILTER_VALIDATE_BOOLEAN)),
          array( 'events' => filter_var($request->getParam('events'), FILTER_VALIDATE_BOOLEAN)),
          array( 'acq' => filter_var($request->getParam('subscription'), FILTER_VALIDATE_BOOLEAN))
        );
      }

      $result = $content->savePreferences($fields, $email);
      $result = json_encode($result);
      $this->_helper->layout->setLayout('service');
      $this->view->assign('response', $result);
      $this->renderScript( 'pages/service.phtml' );
      return;
    }
    else {
      $this->_redirect('/newsletters/');
      return;
    }
  }

  public function premiumAction() {
    $config = Zend_Registry::get('config');
    $newslettersPremium = simplexml_load_file(APPLICATION_PATH.'/configs/newsletters-premium.xml');
    $this->view->assign( 'newsletters', $newslettersPremium );
    $this->view->assign('marketName', $config['market']['title']);
    $this->renderScript( 'pages/newsletters-premium.phtml' );
    return;
  }

  public function unsubscribeAction() {
    $config = Zend_Registry::get('config');
    $request = $this->getRequest();
    $content = Application_Model_Feed::create('newsletters');

    $email = $request->getParam('u');
    $lid = $request->getParam('lid');
    if(empty($email) && empty($lid)) {
      $baseUrl = new Zend_View_Helper_BaseUrl();
      $this->getResponse()->setRedirect($baseUrl->baseUrl().'/newsletters/');
      return;
    }

    if(!empty($config['newsletters']['hide_menu'])) {
      $hideMenu = filter_var($config['newsletters']['hide_menu'], FILTER_VALIDATE_BOOLEAN);
      $this->view->assign('hideMenu', $hideMenu);
    }

    $bannerType = $config['newsletters']['banner'];
    $bannerType = (!empty($bannerType)) ? $bannerType : 'featured';
    $this->view->assign('bannerType', $bannerType);
    $this->view->assign('marketTitle', $config['market']['title']);
    $this->view->assign('marketData', $content->getMarket());
    $this->view->assign('marketName', $config['market']['title']);
    $this->view->assign('heading', $config['market']['title']);
    $this->view->assign('email', $email);
    $this->view->assign('lid', $lid);
    $this->renderScript( 'pages/newsletters-unsubscribe.phtml' );
    return;
  }

  public function subscribeAction() {
    $config = Zend_Registry::get('config');
    $user = Zend_Registry::get('user');
    $request = $this->getRequest();
    $content = Application_Model_Feed::create('newsletters');
    $isLoggedIn = Zend_Registry::get('isloggedin');

    $lid = $request->getParam('lid');
    if(empty($lid)) {
      $baseUrl = new Zend_View_Helper_BaseUrl();
      $this->getResponse()->setRedirect($baseUrl->baseUrl().'/newsletters/');
      return;
    }
    $email = null;
    if($isLoggedIn) {
      $email = $user->getEmail();
      $this->view->assign('status', 'loggedin');
    }
    else {
      $this->view->assign('status', null);
    }

    if(!empty($config['newsletters']['hide_menu'])) {
      $hideMenu = filter_var($config['newsletters']['hide_menu'], FILTER_VALIDATE_BOOLEAN);
      $this->view->assign('hideMenu', $hideMenu);
    }

    $newsletters = $content->getNewsletters();
    $newsletter = $content->getNewsletter($newsletters, $lid, $email);
    $bannerType = $config['newsletters']['banner'];
    $bannerType = (!empty($bannerType)) ? $bannerType : 'featured';
    $this->view->assign('bannerType', $bannerType);
    $this->view->assign('sitekey', $config['recaptcha']['sitekey']);
    $this->view->assign('marketTitle', $config['market']['title']);
    $this->view->assign('marketData', $content->getMarket());
    $this->view->assign('marketName', $config['market']['title']);
    $this->view->assign('heading', $config['market']['title']);
    $this->view->assign('newsletter', $newsletter);
    $this->view->assign('email', $email);
    $this->view->assign('lid', $lid);
    $this->renderScript( 'pages/newsletters-subscribe.phtml' );
    return;
  }
}
