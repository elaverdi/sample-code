<?php
class Zend_View_Helper_RenderNewsletterMenu extends Zend_View_Helper_Abstract {

  public function renderNewsletterMenu($categories, $type = null) {
		?>
			<?php if(!$categories): ?>

			<a class="button hide-for-xlarge return-button" href="/newsletters/"><i class="fa fa-arrow-left"></i> &nbsp; Newsletters</a>
      <ul class="menu vertical show-for-xlarge">
        <li><a href="/newsletters/">Newsletters</a></li>
        <?php if(!empty($type) && $type == 'preferences'): ?>
          <?php if(Zend_Registry::get('isloggedin')): ?>
          <li><a class="selected" href="/newsletters/preferences/">Email Preferences <i class="fa fa-sliders"></i></a></li>
          <?php endif;?>
        <?php elseif (!empty($type) && $type == 'unsubscribe'): ?>
          <li><a class="selected" href="#">Unsubscribe <i class="fa fa-sliders"></i></a></li>
        <?php elseif (!empty($type) && $type == 'subscribe'): ?>
          <li><a class="selected" href="#">Subscribe <i class="fa fa-sliders"></i></a></li>
        <?php endif; ?>
      </ul>
			<?php else:?>
				<a class="button hide-for-xlarge filter-button" href="">Filter</a>
				<ul class="menu vertical sticky" data-sticky data-margin-top="0" data-stick-to="top" data-anchor="newsletters" data-sticky-on="xlarge">
					<li class="hide-for-xlarge"><a class="close-button" href=""><i class="fa fa-close"></i></a></li>
					<li><a class="filter-link-all selected" href="#all">ALL</a></li>
					<?php if($this->view->status === 'loggedin' || $this->view->status === 'loggedin bypass') : ?>
					<li><a class="filter-link-my" href="#section-0">My Newsletters</a></li>
					<?php endif; ?>
					<li><hr /></li>
					<?php foreach($categories as $category): ?>
					<?php if($category['order'] === 0) continue; ?>
					<li><a class="filter-link" href="#section-<?=$category['order']?>"><?=$category['name']?><i class="fa fa-close"></i></a></li>
					<?php endforeach; ?>
					<?php if($this->view->status === 'loggedin') : ?>
					<li><hr /></li>
					<li><a class="preference-link" href="/newsletters/preferences/">Email Preferences <i class="fa fa-sliders"></i></a></li>
					<?php endif; ?>
					<li class="hide-for-xlarge"><a class="button expanded apply-button" href="">Apply</a></li>
				</ul>
			<?php endif;?>
    <?php
  }
  public function renderNewsletterMenu2() {
    echo 'test';
  }
}
?>
